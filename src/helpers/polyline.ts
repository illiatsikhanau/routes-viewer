import {decode} from '@googlemaps/polyline-codec';
import {LatLngTuple} from 'leaflet';

/**
 * Decodes route with @googlemaps/polyline-codec.
 * @param encodedRoute encoded route points from api
 * @return route points
 */
const decodeRoute = (encodedRoute: string): LatLngTuple[] => {
  return decode(encodedRoute);
}

export default decodeRoute;
