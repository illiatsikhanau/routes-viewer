import {Route} from '../types/Route';

/** Routes action types. */
export const Types = {
  /** Set currently displayed route. */
  SET_SELECTED_ROUTE: 'SET_SELECTED_ROUTE',
  /** Routes fetch request. */
  FETCH_ROUTES_REQUEST: 'FETCH_ROUTES_REQUEST',
  /** Routes fetch request completed successfully. */
  FETCH_ROUTES_SUCCESS: 'FETCH_ROUTES_SUCCESS',
  /** Routes fetch request ends with error. */
  FETCH_ROUTES_FAILURE: 'FETCH_ROUTES_FAILURE',
};

/**
 * Set currently displayed route.
 * @param route route to display
 */
export const setSelectedRoute = (route: Route) => ({
  type: Types.SET_SELECTED_ROUTE,
  payload: {route}
});

/**
 * Routes fetch request.
 * @param params inline points coords for api
 */
export const fetchRoutesRequest = (params: string) => ({
  type: Types.FETCH_ROUTES_REQUEST,
  payload: {params}
});

/**
 * Routes fetch request completed successfully.
 * @param encodedRoute encoded route points from api
 */
export const fetchRoutesSuccess = (encodedRoute: string) => ({
  type: Types.FETCH_ROUTES_SUCCESS,
  payload: {encodedRoute}
});

/**
 * Routes fetch request ends with error.
 */
export const fetchRoutesFailure = () => ({
  type: Types.FETCH_ROUTES_FAILURE
});
