import {Route} from '../types/Route';
import {Types} from '../actions/routes';
import {LatLngTuple} from 'leaflet';
import decodeRoute from '../helpers/polyline';

// all available routes
const routes = [{
  name: 'Route №1',
  points: [
    {latitude: 59.84660399, longitude: 30.29496392},
    {latitude: 59.82934196, longitude: 30.42423701},
    {latitude: 59.83567701, longitude: 30.38064206},
  ],
}, {
  name: 'Route №2',
  points: [
    {latitude: 59.82934196, longitude: 30.42423701},
    {latitude: 59.82761295, longitude: 30.41705607},
    {latitude: 59.84660399, longitude: 30.29496392},
  ],
}, {
  name: 'Route №3',
  points: [
    {latitude: 59.83567701, longitude: 30.38064206},
    {latitude: 59.84660399, longitude: 30.29496392},
    {latitude: 59.82761295, longitude: 30.41705607},
  ],
}];

const initialState: {
  // routes
  list: Route[],
  // currently displayed route
  selectedRoute: Route,
  // currently displayed route points from api
  selectedRoutePoints: LatLngTuple[],
} = {
  list: routes,
  selectedRoute: routes[0],
  selectedRoutePoints: [[routes[0].points[0].latitude, routes[0].points[0].longitude]],
};

export const routesReducer = (state = initialState, action: any) => {
  switch (action.type) {
    // update selectedRoute
    case Types.SET_SELECTED_ROUTE:
      return {
        ...state,
        selectedRoute: action.payload.route
      };
    // update selectedRoutePoints from api
    case Types.FETCH_ROUTES_SUCCESS:
      return {
        ...state,
        selectedRoutePoints: decodeRoute(action.payload.encodedRoute)
      };
    // report an error
    case Types.FETCH_ROUTES_FAILURE:
      alert('Service unavailable; try again later.');
      return state;
    default:
      return state;
  }
};
