import React from 'react';
import ReactDOM from 'react-dom/client';
import reducer from './reducers';
import rootSaga from './sagas';
import createSagaMiddleware from 'redux-saga';
import {configureStore} from '@reduxjs/toolkit';
import {Provider, TypedUseSelectorHook, useSelector} from 'react-redux';
import App from './components/App';

const sagaMiddleware = createSagaMiddleware();
const store = configureStore({
  reducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(sagaMiddleware),
});
sagaMiddleware.run(rootSaga);

export const selector: TypedUseSelectorHook<ReturnType<typeof store.getState>> = useSelector;

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <Provider store={store}>
    <App/>
  </Provider>
);
