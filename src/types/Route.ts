import {Point} from './Point';

// Map route
export interface Route {
  // name for displaying in routes list
  name: string,
  // map points coordinates
  points: Point[],
}
