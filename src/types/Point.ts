// Map coordinate
export interface Point {
  latitude: number,
  longitude: number
}
