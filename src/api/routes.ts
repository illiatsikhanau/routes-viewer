import axios from 'axios';

/**
 * Gets route from api between points.
 * @param params inline points coords for api
 */
export const getEncoredRoute = (params: string) => {
  return axios
    .get(`https://router.project-osrm.org/route/v1/driving/${params}`)
    .then(result => result.data?.routes[0]?.geometry)
    .catch(() => alert('Service unavailable; try again later.'));
}
