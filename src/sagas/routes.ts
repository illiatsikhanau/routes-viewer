import {takeLatest, call, put, fork} from 'redux-saga/effects';
import * as actions from '../actions/routes';
import * as api from '../api/routes';
import {PayloadAction} from "@reduxjs/toolkit";

/**
 * Worker saga.
 * Will be fired on FETCH_ROUTES_REQUEST actions
 * @param action action with {params: inline points coords for api}
 */
function* fetchRoutes(action: PayloadAction<{params: string}>): Generator {
  try {
    // trying to get encored route from api...
    const encoredRoute = yield call(api.getEncoredRoute, action.payload.params);
    if (encoredRoute) {
      yield put(actions.fetchRoutesSuccess(encoredRoute as string));
    } else {
      yield put(actions.fetchRoutesFailure());
    }
  } catch {
    yield put(actions.fetchRoutesFailure());
  }
}

/**
 * Watcher saga.
 * Spawns a new fetchRoutes task on each FETCH_ROUTES_REQUEST action
 */
function* watchFetchRoutesRequest() {
  yield takeLatest(actions.Types.FETCH_ROUTES_REQUEST, fetchRoutes);
}

const routesSagas = [fork(watchFetchRoutesRequest)];

export default routesSagas;
