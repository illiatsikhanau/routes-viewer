import {all} from 'redux-saga/effects';
import routesSagas from './routes';

export default function* rootSaga() {
  yield all([...routesSagas]);
}
