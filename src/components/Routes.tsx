import './Routes.scss';
import React from 'react';
import {useDispatch} from 'react-redux';
import {selector} from '../index';
import {setSelectedRoute} from '../actions/routes';
import {Route} from '../types/Route';
import {Table} from 'antd';

// antd table columns
const columns = [{
  title: 'Route',
  dataIndex: 'name',
}, {
  title: 'Point 1 (lat, lon)',
  dataIndex: 'point1',
  render: (text: string, route: Route) => `${route.points[0].latitude}, ${route.points[0].longitude}`
}, {
  title: 'Point 2 (lat, lon)',
  dataIndex: 'point2',
  render: (text: string, route: Route) => `${route.points[1].latitude}, ${route.points[1].longitude}`
}, {
  title: 'Point 3 (lat, lon)',
  dataIndex: 'point3',
  render: (text: string, route: Route) => `${route.points[2].latitude}, ${route.points[2].longitude}`
}];

const Routes = () => {
  const dispatch = useDispatch();
  // all available routes
  const routes = selector((state) => state.routes.list);
  // currently displayed route
  const route = selector((state) => state.routes.selectedRoute);

  return (
    <Table
      className='routes'
      dataSource={routes}
      columns={columns}
      rowKey='name'
      // row selection params
      rowSelection={{
        selectedRowKeys: [route.name],
        type: 'radio',
      }}
      // on click row
      onRow={(route, _) => {
        return {
          onClick: _ => dispatch(setSelectedRoute(route))
        }
      }}
      pagination={false}
      bordered
    />
  );
}

export default Routes;
