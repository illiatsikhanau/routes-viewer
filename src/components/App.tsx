import './App.scss';
import React from 'react';
import Routes from './Routes';
import Map from './Map';

const App = () => {
  return (
    <div className='app'>
      <Routes/>
      <Map/>
    </div>
  );
}

export default App;
