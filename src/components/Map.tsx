import './Map.scss';
import React, {useState, useEffect} from 'react';
import {useDispatch} from 'react-redux';
import {fetchRoutesRequest} from '../actions/routes';
import L, {LatLngExpression} from 'leaflet';
import {MapContainer, TileLayer, useMap} from 'react-leaflet';
import {selector} from '../index';
import {Point} from '../types/Point';

// default map point
const emptyPoint: LatLngExpression = [0, 0];
// custom marker icon
const markerIcon = L.icon({
  iconUrl: 'marker.svg',
  iconSize: [48, 48],
  iconAnchor: [24, 44],
});

const Layer = () => {
  const dispatch = useDispatch();
  // currently displayed route
  const route = selector((state) => state.routes.selectedRoute);
  // points from api
  const points = selector((state) => state.routes.selectedRoutePoints);

  // leaflet map
  const map = useMap();
  // route polyline from route points
  const [polyline, setPolyline] = useState(L.polyline([emptyPoint]).addTo(map));
  // route polyline from route points
  const [markers, setMarkers] = useState([L.marker(emptyPoint, {icon: markerIcon}).addTo(map)]);

  // on currently displayed route change
  useEffect(() => {
    // get new route from api
    const params = route.points.map((point: Point) => `${point.longitude},${point.latitude}`).join(';');
    dispatch(fetchRoutesRequest(params));
  }, [dispatch, route]);

  // on route points change
  useEffect(() => {
    // create fake markers from points to change map bounds
    const routeMarkers = points.map(point => L.marker([point[0], point[1]]));
    map.fitBounds(L.featureGroup(routeMarkers).getBounds());

    // remove previous polyline and create new one
    polyline.remove();
    setPolyline(L.polyline(points).addTo(map));

    // remove previous markers and create new from points
    markers.forEach(marker => marker.remove());
    setMarkers(route.points.map((point: Point, index: number) =>
      L.marker([point.latitude, point.longitude], {icon: markerIcon}).addTo(map).bindPopup(`Point ${index + 1}`)
    ));

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [points])

  return (
    <TileLayer
      attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
      url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
    />
  );
}

const Map = () => {
  return (
    <MapContainer className='map' center={emptyPoint} zoom={12}>
      <Layer/>
    </MapContainer>
  );
}

export default Map;
